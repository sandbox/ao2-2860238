Entity translation switcher
===========================

This module provides links to switch between entity translations.

This could be used to change the content language independently from the
interface language when a suitable **content** language negotiation mechanism is
in place.

Installation
------------

Enable the module and configure the entity translation switcher field in the
"Manage display" tab of the content type (e.g. for the "page" content type:
admin/structure/types/manage/page/display).
